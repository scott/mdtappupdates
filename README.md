To get started, follow instructions in SharePoint to setup your SSH key, and install Git.
https://virginiatech.sharepoint.com/sites/ag/calsit/SitePages/guides/GitLab.aspx


Run Command line Git, and navigate to the folder you want to clone the repository.

>git clone git@code.vt.edu:scott/mdtappupdates

This will copy the current repository to your local computer.

Copy the data you want to transfer into that folder.
>git add [filename]
or for all files
>git add .

Commit the changes with a Change comment:
>git commit -m "Initial commit"

Then upload all the file to the repository:
>git push -u origin master

If changes are made by editing from Code.vt.edu web interface or if someone else makes a change, you will need to pull down those changes first:
>git pull

